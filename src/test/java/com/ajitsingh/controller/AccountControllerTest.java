package com.ajitsingh.controller;

import com.ajitsingh.dtos.request.AccountDTO;
import com.ajitsingh.dtos.request.MoneyTransferDTO;
import com.ajitsingh.dtos.response.AccountResponseDTO;
import com.ajitsingh.model.Account;
import com.ajitsingh.model.Bank;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(DropwizardExtensionsSupport.class)
public class AccountControllerTest {

    private static class DummyExceptionMapper implements ExceptionMapper<WebApplicationException> {
        @Override
        public Response toResponse(WebApplicationException e) {
            throw new UnsupportedOperationException();
        }
    }

    private static final ObjectMapper OBJECT_MAPPER = Jackson.newObjectMapper()
            .registerModule(new GuavaModule());


    private ResourceExtension resources = ResourceExtension.builder()
            .addResource(new AccountController())
            .setMapper(OBJECT_MAPPER)
            .setClientConfigurator(clientConfig -> clientConfig.register(DummyExceptionMapper.class))
            .build();

    @Test
    void shouldCreateAccountForValidRequest() {
        AccountDTO accountDTO = new AccountDTO("Ajit", "Singh", new BigDecimal(500));

        AccountResponseDTO response = resources.target("/account")
                .request()
                .post(Entity.json(accountDTO))
                .readEntity(AccountResponseDTO.class);

        Account account = Bank.getInstance().getAccount(response.getAccountNumber());

        assertThat(account).isNotNull();
        assertThat(account.getBalance()).isEqualTo(accountDTO.getInitialAmount());
    }

    @Test
    void shouldNotCreateAccountWhenFirstNameIsNotPresent() {
        AccountDTO accountDTO = new AccountDTO("", "Singh", new BigDecimal(500));

        int status = resources.target("/account")
                .request()
                .post(Entity.json(accountDTO))
                .getStatus();

        assertThat(status).isEqualTo(422);
    }

    @Test
    void shouldNotCreateAccountWhenLastNameIsNotPresent() {
        AccountDTO accountDTO = new AccountDTO("Ajit", "", new BigDecimal(500));

        int status = resources.target("/account")
                .request()
                .post(Entity.json(accountDTO))
                .getStatus();

        assertThat(status).isEqualTo(422);
    }

    @Test
    void shouldNotCreateAccountWhenInitialAmountIsLessThanZero() {
        AccountDTO accountDTO = new AccountDTO("Ajit", "Singh", new BigDecimal(-1));

        int status = resources.target("/account")
                .request()
                .post(Entity.json(accountDTO))
                .getStatus();

        assertThat(status).isEqualTo(422);
    }

    @Test
    void shouldGetAccountInformation() {
        AccountDTO accountDTO = new AccountDTO("Ajit", "Singh", new BigDecimal(500));

        Account account = Bank.getInstance().createAccount(accountDTO);

        AccountResponseDTO response = resources.target("/account/" + account.getNumber())
                .request()
                .get()
                .readEntity(AccountResponseDTO.class);

        assertThat(response).isNotNull();
        assertThat(response.getBalance()).isEqualTo(accountDTO.getInitialAmount());
        assertThat(response.getAccountNumber()).isEqualTo(account.getNumber());
    }

    @Test
    void shouldTransferMoneyFromOneAccountToAnother() {
        AccountDTO accountDTO = new AccountDTO("Ajit", "Singh", new BigDecimal(500));
        AccountDTO beneficiaryAccountDTO = new AccountDTO("Jake", "Throne", new BigDecimal(600));

        Account account = Bank.getInstance().createAccount(accountDTO);
        Account beneficiaryAccount = Bank.getInstance().createAccount(beneficiaryAccountDTO);

        MoneyTransferDTO moneyTransferDTO = new MoneyTransferDTO(beneficiaryAccount.getNumber(), new BigDecimal(100));

        int status = resources.target("/account/" + account.getNumber())
                .request()
                .put(Entity.json(moneyTransferDTO))
                .getStatus();

        assertThat(status).isEqualTo(202);
        assertThat(beneficiaryAccount.getBalance()).isEqualTo(new BigDecimal(700));
        assertThat(account.getBalance()).isEqualTo(new BigDecimal(400));
    }

    @Test
    void shouldNotTransferMoneyWhenAccountDoesNotHaveSufficientMoney() {
        AccountDTO accountDTO = new AccountDTO("Ajit", "Singh", new BigDecimal(500));
        AccountDTO beneficiaryAccountDTO = new AccountDTO("Jake", "Throne", new BigDecimal(600));

        Account account = Bank.getInstance().createAccount(accountDTO);
        Account beneficiaryAccount = Bank.getInstance().createAccount(beneficiaryAccountDTO);

        MoneyTransferDTO moneyTransferDTO = new MoneyTransferDTO(beneficiaryAccount.getNumber(), new BigDecimal(501));

        int status = resources.target("/account/" + account.getNumber())
                .request()
                .put(Entity.json(moneyTransferDTO))
                .getStatus();

        assertThat(status).isEqualTo(422);
        assertThat(beneficiaryAccount.getBalance()).isEqualTo(new BigDecimal(600));
        assertThat(account.getBalance()).isEqualTo(new BigDecimal(500));
    }

    @Test
    void shouldNotTransferMoneyWhenTransferAmountIsLessThanOne() {
        AccountDTO accountDTO = new AccountDTO("Ajit", "Singh", new BigDecimal(500));
        AccountDTO beneficiaryAccountDTO = new AccountDTO("Jake", "Throne", new BigDecimal(600));

        Account account = Bank.getInstance().createAccount(accountDTO);
        Account beneficiaryAccount = Bank.getInstance().createAccount(beneficiaryAccountDTO);

        MoneyTransferDTO moneyTransferDTO = new MoneyTransferDTO(beneficiaryAccount.getNumber(), new BigDecimal(0));

        int status = resources.target("/account/" + account.getNumber())
                .request()
                .put(Entity.json(moneyTransferDTO))
                .getStatus();

        assertThat(status).isEqualTo(422);
        assertThat(beneficiaryAccount.getBalance()).isEqualTo(new BigDecimal(600));
        assertThat(account.getBalance()).isEqualTo(new BigDecimal(500));
    }
}