package com.ajitsingh.model;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

class AccountTest {

    @Test
    void shouldWithdrawMoneyFromAccount() {
        User user = new User("Ajit", "Singh");
        Account account = new Account(user, "123456", new BigDecimal(500));

        assertThat(account.withdraw(new BigDecimal(200))).isTrue();
        assertThat(account.getBalance()).isEqualTo(new BigDecimal(300));
    }

    @Test
    void shouldNotWithdrawMoneyFromAccountWhenWithdrawAmountIsMoreThanBalance() {
        User user = new User("Ajit", "Singh");
        Account account = new Account(user, "123456", new BigDecimal(500));

        assertThat(account.withdraw(new BigDecimal(501))).isFalse();
        assertThat(account.getBalance()).isEqualTo(new BigDecimal(500));
    }

    @Test
    void shouldCheckThatWithdrawIsThreadSafe() throws Exception {
        User user = new User("Ajit", "Singh");
        Account account = new Account(user, "123456", new BigDecimal(300));

        //30 parallel transactions of $10 each
        CompletableFuture<Void> futures = CompletableFuture.allOf(
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account),
                withdraw(account), withdraw(account), withdraw(account)
        );

        futures.get(10, TimeUnit.SECONDS);

        assertThat(account.getBalance()).isEqualTo(new BigDecimal(0));
    }

    @Test
    void shouldCreditAmountToAccount() {
        User user = new User("Ajit", "Singh");
        Account account = new Account(user, "123456", new BigDecimal(500));

        account.credit(new BigDecimal(100));

        assertThat(account.getBalance()).isEqualTo(new BigDecimal(600));
    }

    private CompletableFuture<Void> withdraw(Account account) {
        Runnable runnable = () -> {
            try {
                account.withdraw(new BigDecimal(10));
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        return CompletableFuture.runAsync(runnable);
    }

}