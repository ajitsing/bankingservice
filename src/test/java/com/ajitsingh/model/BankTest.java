package com.ajitsingh.model;

import com.ajitsingh.dtos.request.AccountDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BankTest {

    private AccountDTO accountDTO;

    @BeforeEach
    void setUp() {
        accountDTO = new AccountDTO("Ajit", "Singh", new BigDecimal(100));
    }

    @Test
    void shouldCreateNewAccount() {
        Account account = Bank.getInstance().createAccount(accountDTO);

        assertThat(account.getBalance()).isEqualTo(accountDTO.getInitialAmount());
        assertThat(account.getNumber()).isNotNull();
    }

    @Test
    void shouldGetCreateAccountInfo() {
        Account account = Bank.getInstance().createAccount(accountDTO);

        Account fetchedAccount = Bank.getInstance().getAccount(account.getNumber());

        assertThat(fetchedAccount.getBalance()).isEqualTo(accountDTO.getInitialAmount());
        assertThat(fetchedAccount.getNumber()).isEqualTo(account.getNumber());
    }

    @Test
    void shouldReturnTrueWhenAccountExists() {
        Account account = Bank.getInstance().createAccount(accountDTO);

        assertThat(Bank.getInstance().hasAccount(account.getNumber())).isTrue();
    }

    @Test
    void shouldReturnFalseWhenAccountDoesNotExists() {
        assertThat(Bank.getInstance().hasAccount("123456")).isFalse();
    }

    @Test
    void shouldTransferMoneyWhenWithdrawFromMainAccountIsSuccessful() {
        AccountDTO beneficiaryAccountDTO = new AccountDTO("Jake", "Thorne", new BigDecimal(100));

        Account account = Bank.getInstance().createAccount(accountDTO);
        Account beneficiaryAccount = Bank.getInstance().createAccount(beneficiaryAccountDTO);

        assertTrue(Bank.getInstance().transfer(account.getNumber(), beneficiaryAccount.getNumber(), new BigDecimal(50)));

        assertThat(account.getBalance()).isEqualTo(new BigDecimal(50));
        assertThat(beneficiaryAccount.getBalance()).isEqualTo(new BigDecimal(150));
    }

    @Test
    void shouldNotTransferMoneyWhenWithdrawFromMainAccountIsUnsuccessful() {
        AccountDTO beneficiaryAccountDTO = new AccountDTO("Jake", "Thorne", new BigDecimal(100));

        Account account = Bank.getInstance().createAccount(accountDTO);
        Account beneficiaryAccount = Bank.getInstance().createAccount(beneficiaryAccountDTO);

        assertFalse(Bank.getInstance().transfer(account.getNumber(), beneficiaryAccount.getNumber(), new BigDecimal(110)));

        assertThat(account.getBalance()).isEqualTo(new BigDecimal(100));
        assertThat(beneficiaryAccount.getBalance()).isEqualTo(new BigDecimal(100));
    }
}