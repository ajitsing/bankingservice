package com.ajitsingh.service;

import com.ajitsingh.dtos.request.AccountDTO;
import com.ajitsingh.dtos.response.AccountResponseDTO;
import com.ajitsingh.model.Account;
import com.ajitsingh.model.Bank;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class AccountServiceTest {

    private AccountService service;
    private Account beneficiaryAccount;
    private Account mainAccount;

    @BeforeEach
    void setUp() {
        service = new AccountService();
        beneficiaryAccount = Bank.getInstance().createAccount(new AccountDTO("Ajit", "Singh", new BigDecimal(400)));
        mainAccount = Bank.getInstance().createAccount(new AccountDTO("Jake", "Throne", new BigDecimal(500)));
    }

    @Test
    void shouldReturn404WhenAccountDoesNotExist() {
        String accountNumber = "123";
        Response response = service.transferMoney(accountNumber, beneficiaryAccount.getNumber(), new BigDecimal(200));

        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.getEntity()).isEqualTo("Account not found");
    }

    @Test
    void shouldReturn404WhenBeneficiaryAccountDoesNotExist() {
        String beneficiaryAccountNumber = "123";
        Response response = service.transferMoney(mainAccount.getNumber(), beneficiaryAccountNumber, new BigDecimal(200));

        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.getEntity()).isEqualTo("Beneficiary account not found");
    }

    @Test
    void shouldReturn422WhenMainAccountDoesNotHaveSufficientMoney() {
        Response response = service.transferMoney(mainAccount.getNumber(), beneficiaryAccount.getNumber(), new BigDecimal(600));

        assertThat(response.getStatus()).isEqualTo(422);
        assertThat(response.getEntity()).isEqualTo("Account does not have sufficient balance");
    }

    @Test
    void shouldTransferMoneyWhenMainAccountHaveSufficientMoney() {
        Response response = service.transferMoney(mainAccount.getNumber(), beneficiaryAccount.getNumber(), new BigDecimal(200));

        assertThat(response.getStatus()).isEqualTo(202);
        assertThat(((AccountResponseDTO) response.getEntity()).getBalance()).isEqualTo(new BigDecimal(300));
    }

    @Test
    void shouldReturn404WhenAccountDoesNotExistWhileGettingAccountInfo() {
        String accountNumber = "123";
        Response response = service.getAccount(accountNumber);

        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.getEntity()).isEqualTo("Account not found");
    }

    @Test
    void shouldReturnAccountInfo() {
        Response response = service.getAccount(mainAccount.getNumber());

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(((AccountResponseDTO) response.getEntity()).getBalance()).isEqualTo(new BigDecimal(500));
    }
}