package com.ajitsingh.controller;

import com.ajitsingh.dtos.request.AccountDTO;
import com.ajitsingh.dtos.request.MoneyTransferDTO;
import com.ajitsingh.dtos.response.AccountResponseDTO;
import com.ajitsingh.model.Account;
import com.ajitsingh.model.Bank;
import com.ajitsingh.service.AccountService;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController {

    private AccountService accountService = new AccountService();

    @GET
    @Path("/{number}")
    public Response getAccount(@PathParam("number") @NotEmpty String accountNumber) {
        return accountService.getAccount(accountNumber);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public AccountResponseDTO openAccount(@Valid AccountDTO accountDTO) {
        Account account = Bank.getInstance().createAccount(accountDTO);
        return new AccountResponseDTO(account);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{number}")
    public Response transferMoney(@PathParam("number") @NotEmpty String accountNumber, @Valid MoneyTransferDTO moneyTransferDTO) {
        String beneficiaryAccountNumber = moneyTransferDTO.getBeneficiaryAccountNumber();
        BigDecimal amount = moneyTransferDTO.getAmount();
        return accountService.transferMoney(accountNumber, beneficiaryAccountNumber, amount);
    }
}
