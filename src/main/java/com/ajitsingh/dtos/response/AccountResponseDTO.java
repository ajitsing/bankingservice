package com.ajitsingh.dtos.response;

import com.ajitsingh.model.Account;

import java.math.BigDecimal;

public class AccountResponseDTO {
    private String accountNumber;
    private BigDecimal balance;

    public AccountResponseDTO(Account account) {
        accountNumber = account.getNumber();
        balance = account.getBalance();
    }

    public AccountResponseDTO() {
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
