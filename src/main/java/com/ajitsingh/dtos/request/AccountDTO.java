package com.ajitsingh.dtos.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class AccountDTO {
    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @Min(0)
    private BigDecimal initialAmount;

    public AccountDTO(String firstName, String lastName, BigDecimal initialAmount) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.initialAmount = initialAmount;
    }

    public AccountDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getInitialAmount() {
        return initialAmount;
    }

    public void setInitialAmount(BigDecimal initialAmount) {
        this.initialAmount = initialAmount;
    }
}
