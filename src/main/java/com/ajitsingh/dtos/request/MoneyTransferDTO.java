package com.ajitsingh.dtos.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class MoneyTransferDTO {
    @NotEmpty
    private String beneficiaryAccountNumber;

    @Min(1)
    private BigDecimal amount;

    public MoneyTransferDTO(String beneficiaryAccountNumber, BigDecimal amount) {
        this.beneficiaryAccountNumber = beneficiaryAccountNumber;
        this.amount = amount;
    }

    public MoneyTransferDTO() {
    }

    public String getBeneficiaryAccountNumber() {
        return beneficiaryAccountNumber;
    }

    public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
        this.beneficiaryAccountNumber = beneficiaryAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
