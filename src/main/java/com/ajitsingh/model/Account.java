package com.ajitsingh.model;

import java.math.BigDecimal;

public class Account {
    private User user;
    private String number;
    private BigDecimal balance;

    public Account(User user, String number, BigDecimal balance) {
        this.user = user;
        this.number = number;
        this.balance = balance;
    }

    public String getNumber() {
        return number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public synchronized boolean withdraw(BigDecimal amount) {
        BigDecimal newBalance = balance.subtract(amount);

        if (newBalance.floatValue() >= new BigDecimal(0).floatValue()) {
            balance = newBalance;
            return true;
        }

        return false;
    }

    public boolean credit(BigDecimal amount) {
        balance = balance.add(amount);
        return true;
    }
}
