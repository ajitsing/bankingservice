package com.ajitsingh.model;

import com.ajitsingh.dtos.request.AccountDTO;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Bank {
    private static Map<String, Account> accounts = new HashMap<>();
    private static volatile Bank bank;

    private Bank() {
    }

    public static Bank getInstance() {
        if (bank == null) {
            synchronized (Bank.class) {
                if (bank == null) { //Double Checked Locking
                    bank = new Bank();
                }
            }
        }

        return bank;
    }

    public Account createAccount(AccountDTO accountDTO) {
        User user = new User(accountDTO.getFirstName(), accountDTO.getLastName());
        String accountNumber = UUID.randomUUID().toString();

        Account account = new Account(user, accountNumber, accountDTO.getInitialAmount());

        accounts.put(account.getNumber(), account);

        return account;
    }

    public Account getAccount(String accountNumber) {
        return accounts.get(accountNumber);
    }

    public boolean transfer(String fromAccountNumber, String beneficiaryAccountNumber, BigDecimal amount) {
        Account account = accounts.get(fromAccountNumber);
        Account beneficiaryAccount = accounts.get(beneficiaryAccountNumber);

        if (account.withdraw(amount)) {
            beneficiaryAccount.credit(amount);
            return true;
        }

        return false;
    }

    public boolean hasAccount(String accountNumber) {
        return accounts.containsKey(accountNumber);
    }
}
