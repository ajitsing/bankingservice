package com.ajitsingh;

import com.ajitsingh.controller.AccountController;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class bankingserviceApplication extends Application<bankingserviceConfiguration> {

    public static void main(final String[] args) throws Exception {
        new bankingserviceApplication().run(args);
    }

    @Override
    public String getName() {
        return "bankingservice";
    }

    @Override
    public void initialize(final Bootstrap<bankingserviceConfiguration> bootstrap) {
        // TODO: application initialization
    }

    @Override
    public void run(final bankingserviceConfiguration configuration, final Environment environment) {
        environment.jersey().register(AccountController.class);
    }

}
