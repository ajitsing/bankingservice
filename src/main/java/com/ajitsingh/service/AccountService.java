package com.ajitsingh.service;

import com.ajitsingh.dtos.response.AccountResponseDTO;
import com.ajitsingh.model.Account;
import com.ajitsingh.model.Bank;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

public class AccountService {
    public Response transferMoney(String accountNumber, String beneficiaryAccountNumber, BigDecimal amount) {
        if (!Bank.getInstance().hasAccount(accountNumber)) {
            return Response.status(404).entity("Account not found").build();
        }

        if (!Bank.getInstance().hasAccount(beneficiaryAccountNumber)) {
            return Response.status(404).entity("Beneficiary account not found").build();
        }

        boolean transferSuccessful = Bank.getInstance().transfer(accountNumber, beneficiaryAccountNumber, amount);

        if (!transferSuccessful) {
            return Response.status(422).entity("Account does not have sufficient balance").build();
        }

        AccountResponseDTO responseDto = new AccountResponseDTO(Bank.getInstance().getAccount(accountNumber));

        return Response.accepted(responseDto).build();
    }

    public Response getAccount(String accountNumber) {
        if (!Bank.getInstance().hasAccount(accountNumber)) {
            return Response.status(404).entity("Account not found").build();
        }

        Account account = Bank.getInstance().getAccount(accountNumber);
        return Response.ok(new AccountResponseDTO(account)).build();
    }
}
