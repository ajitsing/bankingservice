**Available Endpoints:**

   **1. POST    /account**

   **payload:**
```JSON
    {"firstName": "Ajit", "lastName": "Singh", "initialAmount": 500}
```

This endpoint provides the functionality to create a new account in the bank. It will return the newly created account as response.
   
   
   **2. GET     /account/{number}**
   
This endpoint provides info about an existing bank account.

   **3. PUT     /account/{number}**

   **payload:**

```JSON
    {"beneficiaryAccountNumber": "388cb7fd-1f31-41e9-b909-ffae55f5900b", "amount": 1000}
```

This endpoint provides the functionality to transfer money from one account to another. It will return the account info of the main account.


**Start Service**

Run below command from root dir to start the service. The latest service jar is available in ```executable``` folder.

```shell
    ./service.sh
```


**Run Tests**

```shell
    mvn test
```

**Create executable jar**
```shell
    mvn package
```